#ifndef COPIEFICHIERSLOGIC_H
#define COPIEFICHIERSLOGIC_H

#include "backmainviewlogic.h"

class CopieFichiersLogic : public BackMainViewLogic
{
    Q_OBJECT
public:
    explicit CopieFichiersLogic(QObject *parent = nullptr);

signals:

    void enableCopyButton(bool enabled);

    void numberOfFilesToCopy(int numberOfFiles);

    void showProgressBar(bool visible);

    void progress(int value);

public slots:

    void askOriginDirectory();

    void askDestinationDirectory();

    void startCopy();

private:

    QString mOriginDirectory;

    QString mDestinationDirectory;

};

#endif // COPIEFICHIERSLOGIC_H
