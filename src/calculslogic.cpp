#include "calculslogic.h"

#include <cmath>

CalculsLogic::CalculsLogic(QObject *parent) : BackMainViewLogic(parent)
{
}

void CalculsLogic::xFonction1Changed(const QString &value)
{
    xFonction1 = value.toDouble();
    computeFonction1();
}

void CalculsLogic::yFonction1Changed(const QString &value)
{
    yFonction1 = value.toDouble();
    computeFonction1();
}

void CalculsLogic::xFonction2Changed(const QString &value)
{
    bool validConversion;
    auto temporaryXFonction2 = value.toDouble(&validConversion);

    if (validConversion) {
        xFonction2 = temporaryXFonction2;
        computePowerOfTwo();
    } else {
        emit resultatFonction2Changed(QString());
    }
}

void CalculsLogic::computeFonction1()
{
    QString result("invalid");

    if (xFonction1 < 0) {
        emit resultatFonction1Changed(result);
        return;
    }

    if (yFonction1 <= 0.000000001 && yFonction1 >= -0.000000001) {
        emit resultatFonction1Changed(result);
        return;
    }

    emit resultatFonction1Changed(QString::number(sqrt(xFonction1) / yFonction1));
}

void CalculsLogic::computePowerOfTwo()
{
    emit resultatFonction2Changed(QString::number(xFonction2 * xFonction2));
}
