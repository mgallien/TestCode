#include "copiefichierslogic.h"

#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QtConcurrent>

CopieFichiersLogic::CopieFichiersLogic(QObject *parent) : BackMainViewLogic(parent)
{
}

void CopieFichiersLogic::askOriginDirectory()
{
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    auto result = dialog.exec();
    if (result == QDialog::Accepted) {
        auto listFiles = dialog.selectedFiles();
        if (listFiles.size() == 1) {
            mOriginDirectory = listFiles.constFirst();
        }
    }

    emit enableCopyButton(!mDestinationDirectory.isNull() && !mOriginDirectory.isNull());
}

void CopieFichiersLogic::askDestinationDirectory()
{
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    auto result = dialog.exec();
    if (result == QDialog::Accepted) {
        auto listFiles = dialog.selectedFiles();
        if (listFiles.size() == 1) {
            mDestinationDirectory = listFiles.constFirst();
        }
    }

    emit enableCopyButton(!mDestinationDirectory.isNull() && !mOriginDirectory.isNull());
}

void CopieFichiersLogic::startCopy()
{
    emit showProgressBar(true);
    emit enableCopyButton(false);

    QtConcurrent::run(QThreadPool::globalInstance(), [=] () {
        QDir originDirectory(mOriginDirectory);
        auto allFiles = originDirectory.entryList(QDir::NoDotAndDotDot | QDir::Files);
        emit numberOfFilesToCopy(allFiles.size());
        int progress = 0;
        emit showProgressBar(progress);
        for (auto oneFile : allFiles) {
            QFile currentFile(mOriginDirectory + "/" + oneFile);
            currentFile.copy(mDestinationDirectory + "/" + oneFile);
            ++progress;
            emit showProgressBar(progress);
        }
        emit showProgressBar(false);
        emit enableCopyButton(!mDestinationDirectory.isNull() && !mOriginDirectory.isNull());
    });
}
