#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <memory>

namespace Ui {

class MainWindow;

class MainView;

class Calculs;

class CopieFichiers;

}

class MainViewLogic;

class CalculsLogic;

class CopieFichiersLogic;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private:

    std::unique_ptr<Ui::MainWindow> ui;

    std::unique_ptr<Ui::MainView> mainViewUi;

    std::unique_ptr<MainViewLogic> mainViewLogic;

    std::unique_ptr<Ui::Calculs> calculsUi;

    std::unique_ptr<CalculsLogic> calculsLogic;

    std::unique_ptr<Ui::CopieFichiers> copieFichiersUi;

    std::unique_ptr<CopieFichiersLogic> copieFichiersLogic;

};

#endif // MAINWINDOW_H
