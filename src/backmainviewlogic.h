#ifndef BACKMAINVIEWLOGIC_H
#define BACKMAINVIEWLOGIC_H

#include "switchviewlogic.h"

class BackMainViewLogic : public SwitchViewLogic
{

    Q_OBJECT

public:

    explicit BackMainViewLogic(QObject *parent = nullptr);

signals:

public slots:

    void backToMainView();

};

#endif // BACKMAINVIEWLOGIC_H
