#include "mainwindow.h"

#include "ui_mainwindow.h"
#include "ui_mainview.h"
#include "ui_calculs.h"
#include "ui_copiefichiers.h"

#include "mainviewlogic.h"
#include "calculslogic.h"
#include "copiefichierslogic.h"
#include "utils.h"

#include <QStackedLayout>
#include <QCoreApplication>
#include <QIntValidator>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mainViewUi(new Ui::MainView),
    mainViewLogic(new MainViewLogic),
    calculsUi(new Ui::Calculs),
    calculsLogic(new CalculsLogic),
    copieFichiersUi(new Ui::CopieFichiers),
    copieFichiersLogic(new CopieFichiersLogic)
{
    ui->setupUi(this);

    delete ui->centralWidget->layout();
    auto *stackLayout = new QStackedLayout;

    auto *mainView = new QWidget(this);
    mainView->setObjectName("mainViewWidget");
    mainViewUi->setupUi(mainView);

    connect(mainViewUi->quitButton, &QPushButton::clicked,
            QCoreApplication::instance(), &QCoreApplication::quit);
    connect(mainViewUi->copieFichiersButton, &QPushButton::clicked,
            mainViewLogic.get(), &MainViewLogic::showCopieFichiers);
    connect(mainViewUi->calculsButton, &QPushButton::clicked,
            mainViewLogic.get(), &MainViewLogic::showCalculs);
    connect(mainViewLogic.get(), &MainViewLogic::switchView,
            stackLayout, &QStackedLayout::setCurrentIndex);

    stackLayout->addWidget(mainView);

    auto *copieFichiersView = new QWidget(this);
    copieFichiersView->setObjectName("copieFichiersViewWidget");
    copieFichiersUi->setupUi(copieFichiersView);
    copieFichiersUi->progressBar->setVisible(false);

    connect(copieFichiersUi->quitButton, &QPushButton::clicked,
            QCoreApplication::instance(), &QCoreApplication::quit);
    connect(copieFichiersUi->backButton, &QPushButton::clicked,
            copieFichiersLogic.get(), &CopieFichiersLogic::backToMainView);
    connect(copieFichiersLogic.get(), &CopieFichiersLogic::switchView,
            stackLayout, &QStackedLayout::setCurrentIndex);
    connect(copieFichiersUi->originDirectoryButton, &QPushButton::clicked,
            copieFichiersLogic.get(), &CopieFichiersLogic::askOriginDirectory);
    connect(copieFichiersUi->destinationDirectoryButton, &QPushButton::clicked,
            copieFichiersLogic.get(), &CopieFichiersLogic::askDestinationDirectory);
    connect(copieFichiersLogic.get(), &CopieFichiersLogic::enableCopyButton,
            copieFichiersUi->startCopyButton, &QPushButton::setEnabled);
    connect(copieFichiersUi->startCopyButton, &QPushButton::clicked,
            copieFichiersLogic.get(), &CopieFichiersLogic::startCopy);
    connect(copieFichiersLogic.get(), &CopieFichiersLogic::showProgressBar,
            copieFichiersUi->progressBar, &QProgressBar::setVisible);
    connect(copieFichiersLogic.get(), &CopieFichiersLogic::numberOfFilesToCopy,
            copieFichiersUi->progressBar, &QProgressBar::setMaximum);

    stackLayout->addWidget(copieFichiersView);

    auto *calculsView = new QWidget(this);
    calculsView->setObjectName("calculsViewWidget");
    calculsUi->setupUi(calculsView);
    calculsUi->xFonction1->setValidator(new QIntValidator);
    calculsUi->yFonction1->setValidator(new QIntValidator);
    calculsUi->xFonction2->setValidator(new QIntValidator);

    connect(calculsUi->quitButton, &QPushButton::clicked,
            QCoreApplication::instance(), &QCoreApplication::quit);
    connect(calculsUi->backButton, &QPushButton::clicked,
            calculsLogic.get(), &CalculsLogic::backToMainView);
    connect(calculsLogic.get(), &CalculsLogic::switchView,
            stackLayout, &QStackedLayout::setCurrentIndex);
    connect(calculsUi->xFonction1, &QLineEdit::textChanged,
            calculsLogic.get(), &CalculsLogic::xFonction1Changed);
    connect(calculsUi->yFonction1, &QLineEdit::textChanged,
            calculsLogic.get(), &CalculsLogic::yFonction1Changed);
    connect(calculsLogic.get(), &CalculsLogic::resultatFonction1Changed,
            calculsUi->resultatFonction1, &QLineEdit::setText);
    connect(calculsUi->xFonction2, &QLineEdit::textChanged,
            calculsLogic.get(), &CalculsLogic::xFonction2Changed);
    connect(calculsLogic.get(), &CalculsLogic::resultatFonction2Changed,
            calculsUi->resultatFonction2, &QLineEdit::setText);

    stackLayout->addWidget(calculsView);

    ui->centralWidget->setLayout(stackLayout);
}

MainWindow::~MainWindow()
{
}
