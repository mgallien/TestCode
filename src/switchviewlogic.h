#ifndef SWITCHVIEWLOGIC_H
#define SWITCHVIEWLOGIC_H

#include <QObject>

class SwitchViewLogic : public QObject
{

    Q_OBJECT

public:

    explicit SwitchViewLogic(QObject *parent = nullptr);

signals:

    void switchView(int index);

public slots:

};

#endif // SWITCHVIEWLOGIC_H
