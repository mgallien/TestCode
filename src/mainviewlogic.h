#ifndef MAINVIEWLOGIC_H
#define MAINVIEWLOGIC_H

#include "switchviewlogic.h"

class MainViewLogic : public SwitchViewLogic
{
    Q_OBJECT
public:

    explicit MainViewLogic(QObject *parent = nullptr);

signals:

public slots:

    void showCalculs();

    void showCopieFichiers();

};

#endif // MAINVIEWLOGIC_H
