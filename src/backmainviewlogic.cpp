#include "backmainviewlogic.h"
#include "utils.h"

BackMainViewLogic::BackMainViewLogic(QObject *parent)
    : SwitchViewLogic(parent)
{
}

void BackMainViewLogic::backToMainView()
{
    emit switchView(ViewIndex::MAIN_VIEW_INDEX);
}
