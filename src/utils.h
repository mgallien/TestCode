#ifndef UTILS_H
#define UTILS_H

namespace ViewIndex {

static const int MAIN_VIEW_INDEX = 0;

static const int COPIE_FICHIERS_VIEW_INDEX = 1;

static const int CALCULS_VIEW_INDEX = 2;

}

#endif // UTILS_H
