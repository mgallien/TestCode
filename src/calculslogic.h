#ifndef CALCULSLOGIC_H
#define CALCULSLOGIC_H

#include "backmainviewlogic.h"

class CalculsLogic : public BackMainViewLogic
{

    Q_OBJECT

public:

    explicit CalculsLogic(QObject *parent = nullptr);

signals:

    void resultatFonction1Changed(const QString &value);

    void resultatFonction2Changed(const QString &value);

public slots:

    void xFonction1Changed(const QString &value);

    void yFonction1Changed(const QString &value);

    void xFonction2Changed(const QString &value);

private:

    void computeFonction1();

    void computePowerOfTwo();

    double xFonction1;

    double yFonction1;

    double xFonction2;

};

#endif // CALCULSLOGIC_H
