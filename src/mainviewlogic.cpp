#include "mainviewlogic.h"
#include "utils.h"

#include <QCoreApplication>

MainViewLogic::MainViewLogic(QObject *parent) : SwitchViewLogic(parent)
{
}

void MainViewLogic::showCalculs()
{
    emit switchView(ViewIndex::CALCULS_VIEW_INDEX);
}

void MainViewLogic::showCopieFichiers()
{
    emit switchView(ViewIndex::COPIE_FICHIERS_VIEW_INDEX);
}
