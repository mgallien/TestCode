#include "test_calculslogic.h"

#include "../src/calculslogic.h"

CalculsLogicTests::CalculsLogicTests(QObject *parent) : QObject(parent)
{
}

void CalculsLogicTests::initTestCase()
{
}

void CalculsLogicTests::testFonction2()
{
    CalculsLogic myLogic;

    QSignalSpy resultatFonction1ChangedSpy(&myLogic, &CalculsLogic::resultatFonction1Changed);
    QSignalSpy resultatFonction2ChangedSpy(&myLogic, &CalculsLogic::resultatFonction2Changed);

    QCOMPARE(resultatFonction1ChangedSpy.count(), 0);
    QCOMPARE(resultatFonction2ChangedSpy.count(), 0);

    myLogic.xFonction2Changed("12");

    QCOMPARE(resultatFonction1ChangedSpy.count(), 0);
    QCOMPARE(resultatFonction2ChangedSpy.count(), 1);

    QCOMPARE(resultatFonction2ChangedSpy.at(0).count(), 1);
    QCOMPARE(resultatFonction2ChangedSpy.at(0).at(0).canConvert<QString>(), true);
    QCOMPARE(resultatFonction2ChangedSpy.at(0).at(0).toString(), QString("144"));
}

void CalculsLogicTests::testInvalidFonction2()
{
    CalculsLogic myLogic;

    QSignalSpy resultatFonction1ChangedSpy(&myLogic, &CalculsLogic::resultatFonction1Changed);
    QSignalSpy resultatFonction2ChangedSpy(&myLogic, &CalculsLogic::resultatFonction2Changed);

    QCOMPARE(resultatFonction1ChangedSpy.count(), 0);
    QCOMPARE(resultatFonction2ChangedSpy.count(), 0);

    myLogic.xFonction2Changed("bib");

    QCOMPARE(resultatFonction1ChangedSpy.count(), 0);
    QCOMPARE(resultatFonction2ChangedSpy.count(), 1);

    QCOMPARE(resultatFonction2ChangedSpy.at(0).count(), 1);
    QCOMPARE(resultatFonction2ChangedSpy.at(0).at(0).canConvert<QString>(), true);
    QCOMPARE(resultatFonction2ChangedSpy.at(0).at(0).toString(), QString());
}

QTEST_GUILESS_MAIN(CalculsLogicTests)
