#ifndef TEST_CALCULSLOGIC_H
#define TEST_CALCULSLOGIC_H


#include <QtTest>

class CalculsLogicTests : public QObject
{
    Q_OBJECT

public:

    CalculsLogicTests(QObject *parent = nullptr);

private Q_SLOTS:

    void initTestCase();

    void testFonction2();

    void testInvalidFonction2();

};

#endif // TEST_CALCULSLOGIC_H
